# `fsljs`

This is the `fsljs` JavaScript library. It contains helper functions and facilitates interaction between the [`fsl-desktop`](https://git.fmrib.ox.ac.uk/fsl/gui/desktop) electron app and the FSL React web-based [GUIs](https://git.fmrib.ox.ac.uk/fsl/gui).

# Development

## Prerequisites

It is assumed that your development machine is either a Mac or Linux machine.

- [nodejs and NPM](https://nodejs.org/en/)

## Setup

Clone and cd into this repo

```bash
git clone https://git.fmrib.ox.ac.uk/fsl/gui/fsljs.git

cd fsljs
```

Install dependencies via npm

```bash
npm install
```

## Publishing

Publishing is a manual process until the proper CI/CD pipeline is in place. To publish, run the following command:

```bash
npm run pub
```

