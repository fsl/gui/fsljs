/**
 * object containing default invertxfm options for convertxfm
 * @memberof fsljs
 */
export const defaultInvertXFMOpts = {
    '-omat': '',
    '-inverse': true,
    'in': '',
}