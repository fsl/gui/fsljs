import { defaultBetOpts } from "./bet";
import { defaultFlirtOpts } from "./flirt";
import { defaultApplyXFMOpts } from "./applyxfm";
import { defaultInvertXFMOpts } from "./invertxfm";
import { defaultConcatXFMOpts } from "./concatxfm";
import { defaultFastOpts } from "./fast";
// Try to get the FSL object from the window object.
// If it doesn't exist, create it.
/**
 * The FSL namespace object. This is the object that is exposed to the renderer process via the context bridge in the preload script from electron.
 * @namespace
 */
const FSL = window.FSL || {};

/**
 * The main FSLJS namespace object.
 * @namespace
 */
export const fsljs = {
    defaultBetOpts,
    defaultFlirtOpts,
    defaultApplyXFMOpts,
    defaultInvertXFMOpts,
    defaultConcatXFMOpts,
    defaultFastOpts,
};

/**
 * Determines if the given value is a function.
 * @param {*} value - The value to check.
 * @returns {boolean} True if the value is a function, false otherwise.
 */
function isFunction(value) {
    return (typeof value === 'function');
}

/**
 * Gets the version of FSL.
 * @async
 * @function
 * @returns {Promise<string>} A promise that resolves to the version of FSL.
 * @example
 * const fslVersion = await fsljs.fslVersion();
 * console.log(fslVersion);
 * // 6.0.6.4
 */
fsljs.fslVersion = async () => {
    if (isFunction(FSL.fslVersion)) {
        return await FSL.fslVersion();
    } else {
        return FSL.fslVersion;
    }
};

fsljs.onMNIReference = async function(mm=2) {
    if (isFunction(FSL.onMNIReference)) {
        return await FSL.onMNIReference(mm);
    } else {
        return FSL.onMNIReference;
    }
}

/**
 * Runs the given command with the given options.
 * @async
 * @function
 * @param {object} runObject - The object containing the command and options to use when running the command.
 * @param {string} runObject.command - The command to run.
 * @param {object} runObject.opts - The options to use when running the command.
 * @returns {Promise<string>} A promise that resolves to a message indicating that the command was run in the main process.
 * @example
 * const runObject = {
 *   command: 'bet',
 *  opts: {
 *    input: 'test.nii.gz',
 *   output: 'test_brain.nii.gz',
 * },
 * };
 * const message = await fsljs.run(runObject);
 * console.log(message);
 */
fsljs.run = async function ({command, opts}){
    if (isFunction(FSL.run)) {
        return await FSL.run({command, opts});
    } else {
        return FSL.run;
    }
}

/**
 * opens a file dialog in the main process
 * @async
 * @function
 * @returns {Promise<Object>} A promise that resolves to an object containing the file path and the file name.
 * @example
 * const resultObject = await fsljs.openFileDialog();
 * console.log(resultObject.filepaths[0]);
 */
fsljs.openFileDialog = async function (){
    if (isFunction(FSL.openFileDialog)) {
        return await FSL.openFileDialog();
    } else {
        return FSL.openFileDialog;
    }
}

/**
 * opens a save file dialog in the main process
 * @async
 * @function
 * @returns {Promise<Object>} A promise that resolves to an object containing the file path and the file name.
 * @example
 * const resultObject = await fsljs.openSaveFileDialog();
 * console.log(resultObject.filepath);
 */
fsljs.openSaveFileDialog = async function (){
    if (isFunction(FSL.openSaveFileDialog)) {
        return await FSL.openSaveFileDialog();
    } else {
        return FSL.openSaveFileDialog;
    }
}

/**
 * opens a standard file dialog in the main process
 * @async
 * @function
 * @returns {Promise<Object>} A promise that resolves to an object containing the file path and the file name.
 * @example
 * const resultObject = await fsljs.openStandardFileDialog();
 * console.log(resultObject.filepaths[0]);
 */
fsljs.openFslStandardFileDialog = async function (){
    if (isFunction(FSL.openFslStandardFileDialog)) {
        return await FSL.openFslStandardFileDialog();
    } else {
        return FSL.openFslStandardFileDialog;
    }
}

/**
 * removes the extension from a string
 * @function
 * @param {string} str - The string to remove the extension from.
 * @param {string} ext - The extension to remove.
 * @returns {string} The string with the extension removed.
 * @example
 * const str = 'test.nii.gz';
 * const strWithoutExt = fsljs.removeExtension(str);
 * console.log(strWithoutExt);
 */
fsljs.removeExtension = function (str, ext='.nii'){
    if (str === '' || str === null) return str;
    let arr = str.split(ext);
    arr.pop();
    return arr.join('');
}

/**
 * creates a command string from the given command and options
 * @async
 * @function
 * @param {object} commandObject - The object containing the command and options to use when creating the command string.
 * @param {string} commandObject.command - The command to use when creating the command string.
 * @param {object} commandObject.opts - The options to use when creating the command string.
 * @returns {Promise<string>} A promise that resolves to the command string.
 * @example
 * const commandObject = {
 *  command: 'bet',
 * opts: {
 *   input: 'test.nii.gz',
 *  output: 'test_brain.nii.gz',
 * },
 * };
 * const commandString = await fsljs.createCommandString(commandObject);
 */
fsljs.createCommandString = async function ({command, opts}){
    if (isFunction(FSL.createCommandString)) {
        // remove any empty strings from the opts
        Object.keys(opts).forEach(key => {
            if (opts[key] === '') {
                opts[key] = null;
            }
        });
        return await FSL.createCommandString({command, opts});
    } else {
        return FSL.createCommandString;
    }
}

/**
 * gets the comms info from the main process
 * @async
 * @function
 * @returns {Promise<Object>} A promise that resolves to an object containing the comms info.
 * @example
 * const commsInfo = await fsljs.getCommsInfo();
 * console.log(commsInfo);
 * // { fileServerPort: 12345, host: 'localhost' }
 */
fsljs.getCommsInfo = async function (){
    if (isFunction(FSL.getCommsInfo)) {
        return await FSL.getCommsInfo();
    } else {
        return FSL.getCommsInfo;
    }
}

fsljs.readHistory = async function (){
    if (isFunction(FSL.readHistory)) {
        return await FSL.readHistory();
    } else {
        return FSL.readHistory;
    }
}

fsljs.webGL2Supported = async function (){
    let canvas = document.createElement('canvas');
    let gl = canvas.getContext('webgl2');
    let supported = gl !== null;
    gl = null;
    // remove the canvas from the DOM
    canvas.remove();
    return supported
}

/**
 * get the basename of a file unix filepath
 * @function
 * @param {string} str - The filepath to get the basename from.
 * @returns {string} The basename of the filepath.
 * @example
 * const str = '/path/to/file.nii.gz';
 * const basename = fsljs.basename(str);
 * console.log(basename);
 * // file.nii.gz
 */
fsljs.basename = function(str){
    if (str === '' || str === null) return str;
    let parts = str.split('/');
    return parts[parts.length - 1];
}

/**
 * get the full dirname of minus the basename of a file
 * @function
 * @param {string} str - The filepath to get the dirname from.
 * @returns {string} The dirname of the filepath.
 * @example
 * const str = '/path/to/file.nii.gz';
 * const dirname = fsljs.dirname(str);
 * console.log(dirname);
 * // /path/to
 * const str2 = '/path/to/';
 * const dirname2 = fsljs.dirname(str2);
 * console.log(dirname2);
 * // /path/to
 */
fsljs.dirname = function(str){
    if (str === '' || str === null) return str;
    let parts = str.split('/');
    parts.pop();
    return parts.join('/');
}

/**
 * join the given strings together with a forward slash
 * @function
 * @param {...string} args - The strings to join together.
 * @returns {string} The joined string.
 * @example
 * const str = fsljs.join('path', 'to', 'file.nii.gz');
 * console.log(str);
 * // path/to/file.nii.gz
 * const str2 = fsljs.join('path', '', 'to', 'file.nii.gz');
 * console.log(str2);
 * // path/to/file.nii.gz
 */
fsljs.join = function(...args){
    // remove any empty strings from the args
    args = args.filter(arg => arg !== '');
    return args.join('/');
}


